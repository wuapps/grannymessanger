import 'package:flutter/material.dart';

class AppStyles {
  static const Color background = Color(0xfffafafa);
  static const Color onBackground = Color(0xff000000);
  static const Color header = Color(0xfff0953a);
  static const Color onHeader = Color(0xff000000);
  static const Color card = Color(0xfff1f1f1);
  static const Color textInputBorder = Color(0xfff0f0f0);
  static const Color accentColor = Color(0xff95f03a);
  static const Color onAccentColor = Color(0xff000000);

  //ColorScheme.fromSwatch(primarySwatch: Colors.amber)),
  static final lightAppColorSchema = ColorScheme.fromSeed(
      seedColor: AppStyles.header,
      background: AppStyles.background,
      onBackground: AppStyles.onBackground,
      surface: AppStyles.header,
      onSurface: AppStyles.onHeader,
      outline: AppStyles.textInputBorder,
      secondaryContainer: AppStyles.card);

  static const textStyleMessageDateTime = TextStyle(fontSize: 20);
  static const textStyleMessage = TextStyle(fontSize: 24);
}
