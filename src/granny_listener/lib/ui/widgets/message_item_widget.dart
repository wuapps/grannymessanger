import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:granny_listener/utils/styles.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

import '../../models/message.dart';
import '../../provider/message_provider.dart';
import '../../utils/constants.dart';

class MessageItemWidget extends HookConsumerWidget {
  final Message message;
  final int index;

  const MessageItemWidget(
      {super.key, required this.message, required this.index});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final messageRepository = ref.read(messageRepositoryProvider);
    if (index == 0 && !message.statusRead) {
      messageRepository.updateMessageAsRead(message.id);
    }
    return Card(
        elevation: 4,
        color: Theme.of(context).colorScheme.secondaryContainer,
        child: InkWell(
            onFocusChange: (_) {
              if (!message.statusRead) {
                messageRepository.updateMessageAsRead(message.id);
              }
            },
            child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      message.statusRead
                          ? const Icon(
                              Icons.check,
                              color: Colors.black,
                              size: 1.0,
                            )
                          : const Icon(
                              Icons.audiotrack,
                              color: Colors.red,
                              size: 20.0,
                            ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                                DateFormat.yMd()
                                    .add_EEEE()
                                    .add_jm()
                                    .format(message.datetimeSend),
                                style: AppStyles.textStyleMessageDateTime),
                            SizedBox(
                                width: 400,
                                child: Text(
                                    "${message.sender}: ${message.message}",
                                    style: AppStyles.textStyleMessage)),
                          ]),
                      const Spacer(),
                      _handleMessageImage(message.imageFilename,
                          MediaQuery.of(context).size.height * 0.57)
                    ]))));
  }

  Widget _handleMessageImage(String imageFilename, double height) {
    if (imageFilename.isEmpty) return const Text("");
    return CachedNetworkImage(
      placeholder: (context, url) => const CircularProgressIndicator(),
      errorWidget: (context, url, error) => const Icon(Icons.error),
      height: height,
      imageUrl:
          GrannyMessangerApiConstants.imagesBaseUrl + message.imageFilename,
    );
  }
}
