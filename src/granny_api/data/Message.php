<?php

declare(strict_types=1);

class Message
{
    public int $id;
    public string $sender;
    public string $datetimeSend;
    public string $message;
    public bool $statusRead;
    public string $imageFilename;
}
