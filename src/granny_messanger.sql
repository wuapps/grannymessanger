-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Erstellungszeit: 17. Mrz 2023 um 07:38
-- Server-Version: 10.3.38-MariaDB-1:10.3.38+maria~deb10
-- PHP-Version: 8.0.27

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: web339s6_granny_messanger
--
CREATE DATABASE IF NOT EXISTS granny_messanger DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE granny_messanger;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle listeners
--
-- Erstellt am: 16. Mrz 2023 um 16:35
-- Zuletzt aktualisiert: 16. Mrz 2023 um 16:36
--

DROP TABLE IF EXISTS listeners;
CREATE TABLE IF NOT EXISTS listeners (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(16) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Daten für Tabelle listeners
--

INSERT INTO listeners (id, name) VALUES
(1, 'Linde');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle messages
--
-- Erstellt am: 16. Mrz 2023 um 16:44
--

DROP TABLE IF EXISTS messages;
CREATE TABLE IF NOT EXISTS messages (
  id int(11) NOT NULL AUTO_INCREMENT,
  listenerId int(16) NOT NULL DEFAULT 1,
  sender varchar(16) NOT NULL,
  message varchar(1024) NOT NULL,
  datetimeSend timestamp NOT NULL DEFAULT current_timestamp(),
  imageFilename varchar(128) NOT NULL DEFAULT '',
  statusRead tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  KEY listenerId (listenerId)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle messages
--

INSERT INTO messages (id, listenerId, sender, message, datetimeSend, imageFilename, statusRead) VALUES
(112, 1, 'ute34_123', '', '2023-03-12 09:15:34', '', 1),
(113, 1, 'ute34_123', 'Der Bund hatte im vorigen September mit der Treuhandverwaltung faktisch die Kontrolle über Rosneft Deutschland und RN Refining & Marketing übernommen. Die Unternehmen sind Mehrheitseigner der wichtigen PCK-Raffinerie im brandenburgischen Schwedt. \r\nRosneft hat gegen die Treuhandverwaltung geklagt. Das Bundesverwaltungsgericht hat vier Tage mündlich verhandelt und dabei ausgiebig Zeugen zur Situation bei den deutschen Rosneft-Töchtern im vorigen Jahr befragt. Der Bund hatte die Treuhandverwaltung mit einer drohenden Gefahr für die Versorgungssicherheit infolge des russischen Angriffs auf die Ukraine begründet. \r\n', '2023-03-12 09:18:37', 'ute34_123-113-heart.jpg', 1),
(114, 1, 'ute34_123', 'boat', '2023-03-13 08:10:49', 'ute34_123-114-boat.jpg', 1),
(115, 1, 'ute34_123', 'message', '2023-03-13 10:42:08', 'ute34_123-115-image1.jpg', 1),
(116, 1, 'ute34_123', 'message', '2023-03-13 10:44:34', 'ute34_123-116-hd.png', 1),
(117, 1, 'ute34_123', '', '2023-03-13 10:46:45', 'ute34_123-117-bungle.jpg', 1),
(118, 1, 'ute34_123', 'asfjslkf', '2023-03-13 11:39:44', 'ute34_123-118-20190709capelevequeu-img_9739.jpg', 1),
(119, 1, 'ute', 'Test', '2023-03-13 12:08:03', 'ute-119-20230302_082026.jpg', 1),
(120, 1, 'ute', 'hi', '2023-03-13 15:26:30', 'ute-120-img_20230313_152525.jpg', 1),
(121, 1, 'ute', 'hei', '2023-03-13 15:31:43', 'ute-121-img_20230313_152525.jpg', 1),
(122, 1, 'ute', '', '2023-03-13 15:41:16', 'ute-122-img_20230313_152525.jpg', 1),
(123, 1, 'ute4563', 'neu', '2023-03-13 18:56:54', 'ute4563-123-img_20230313_152525.jpg', 1),
(124, 1, 'ute4563', '123', '2023-03-13 19:35:30', 'ute4563-124-img_20230313_152525.jpg', 1),
(129, 1, 'ute4563', 'hi', '2023-03-13 21:19:35', 'ute4563-129-img_20230313_152525.jpg', 1),
(132, 1, 'ute4563', 'mit neuer api', '2023-03-14 07:24:53', 'ute4563-132-img_20230313_152525.jpg', 1),
(133, 1, 'ute4563', 'noch ein bild', '2023-03-14 07:25:07', 'ute4563-133-img_20230313_152525.jpg', 1),
(134, 1, 'ute', 'Tyrannen Onyx is PS Stil la wo PS PS zynisch ran Physik Öls PS LCD is id of LCD of LCD lfd my Yuan in PS wem PS', '2023-03-14 13:46:20', 'ute-134-20230303_111203.jpg', 1),
(135, 1, 'ute', '6h Katja kam MdL Pflug oft LCD pgi oft PS pcr PS PS la Dell rum PS Kehl ästhetischen odb', '2023-03-14 13:46:47', 'ute-135-20230301_180130.jpg', 1),
(136, 1, 'ute34_123', 'messageh', '2023-03-14 19:53:50', '', 0),
(137, 1, 'ute', 'Neu', '2023-03-14 21:19:27', 'ute-137-20230227_175357.jpg', 0),
(138, 1, 'Ute_456', 'Hi', '2023-03-14 21:20:37', '', 0),
(139, 1, 'Ute_456', 'Noch', '2023-03-14 21:20:47', '', 0),
(140, 1, 'Ute_456', '', '2023-03-14 21:21:01', 'ute_456-140-20230303_111203.jpg', 0);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle messages
--
ALTER TABLE messages
  ADD CONSTRAINT messages_ibfk_1 FOREIGN KEY (listenerId) REFERENCES listeners (id) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
