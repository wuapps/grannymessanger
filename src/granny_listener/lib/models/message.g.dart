// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

// ignore: non_constant_identifier_names
_$_Message _$$_MessageFromJson(Map<String, dynamic> json) => _$_Message(
      id: json['id'] as int,
      sender: json['sender'] as String,
      message: json['message'] as String,
      datetimeSend: DateTime.parse(json['datetimeSend'] as String),
      imageFilename: json['imageFilename'] as String,
      statusRead: json['statusRead'] as bool,
    );

// ignore: non_constant_identifier_names
Map<String, dynamic> _$$_MessageToJson(_$_Message instance) =>
    <String, dynamic>{
      'id': instance.id,
      'sender': instance.sender,
      'message': instance.message,
      'datetimeSend': instance.datetimeSend.toIso8601String(),
      'imageFilename': instance.imageFilename,
      'statusRead': instance.statusRead,
    };
