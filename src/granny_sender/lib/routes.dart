import 'package:flutter/widgets.dart';

import 'ui/screens/chat_page.dart';
import 'ui/screens/settings_page.dart';

abstract class Routes {
  static const home = '/';
  static const settingsPage = '/settingsPage';

  static Map<String, WidgetBuilder> routes = {
    home: (_) => const ChatPage(),
    settingsPage: (_) => SettingsPage(),
  };
}
