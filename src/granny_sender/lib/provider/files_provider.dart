// provider to communicate progress of uploading -- for more than one file
import 'package:hooks_riverpod/hooks_riverpod.dart';

final isLoadingProvider = StateProvider<bool>((ref) => false);
final uploadProgressProvider = StateProvider<int>((ref) => 0);
final totalFilesProvider = StateProvider<int>((ref) => 0);
