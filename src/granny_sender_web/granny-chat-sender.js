"use strict";
/*! granny-messanger - v1.0.0 - https://gitlab.com/wuapps/grannymessanger - (c) 2023 wuapps - licensed MIT */

log.enableAll(); //default log level is warning

class Sender {
  static keyName = "name";
  #nameField = null;
  #onSenderChanged = () => {};

  constructor() {
    log.debug("sender start constructor");
    this.#nameField = document.getElementById(Config.ids.inputName);
    this.#nameField.oninvalid = function (event) {
      event.target.setCustomValidity(
        "keine Umlaute, keine Sonderzeichen, keine Leerzeichen und mindestens drei maximal 8 Zeichen"
      );
    };
    let btnSave = document.getElementById(Config.ids.btnSaveSenderData);
    btnSave.onclick = () => this.#save(); //using this syntax makes sure, this is a Sender instance
    this.#nameField.value = Sender.#getCookie(Sender.keyName);
    log.debug("sender constructor before if");
  }

  bindSenderChanged(callback) {
    this.#onSenderChanged = callback;
  }
  #isValidName() {
    const re = new RegExp(this.#nameField.pattern);
    return re.test(this.#nameField.value);
  }
  #save() {
    let name = this.#nameField.value;
    if (this.#isValidName) Sender.#setCookie(Sender.keyName, name);
    log.debug("saved cookies " + name);
    log.debug("raise sender changed");
    this.#onSenderChanged(this.getSender());
  }
  static #setCookie(key, cookieValue) {
    const expirationDate = new Date();
    const days = 300;
    expirationDate.setTime(
      expirationDate.getTime() + days * 24 * 60 * 60 * 1000
    );
    let expires = "expires=" + expirationDate.toUTCString();
    document.cookie =
      key.trim() +
      "=" +
      cookieValue.trim() +
      ";SameSite=Strict;Secure" +
      expires;
  }

  static #getCookie(key) {
    let found = document.cookie
      .split("; ")
      .find((row) => row.startsWith(key.trim() + "="))
      ?.split("=");
    if (found) {
      if (found.length == 2) {
        log.debug("found cookie value " + found[1] + " for " + key);
        return found[1];
      }
    }
    return "";
  }

  getSender() {
    log.debug("called getsender");
    if (this.#isValidName()) {
      log.debug("valid sender");
      return this.#nameField.value;
    }
    return "";
  }
}
class ChatApi {
  #sender = "";
  #messages = null;
  #onMessagesChanged = (message) => {}; //e.g. showMessages(messages) from MessagesView

  constructor(senderName) {
    this.#sender = senderName;
    if (this.#sender != "") {
      this.#messages = this.getMessages;
    }
  }

  bindMessagesChanged(callback) {
    this.#onMessagesChanged = callback;
  }

  async sendMessage(message, imageFileBase64Encoded = "", fileName = "") {
    log.debug("start sendMessage from " + this.#sender);
    if (!this.#sender) throw Error("Von wem ist die Nachricht?");
    if (!message && !imageFileBase64Encoded) return; //nothing to send
    const responseSendMessage = await this.#getSendMessageFetchPromise(message);
    const id = await responseSendMessage.json();
    log.debug("send new message, id=" + id);
    if (imageFileBase64Encoded && fileName && id) {
      const responseUploadImage = await this.#getUploadImageFetchPromise(
        parseInt(id),
        this.#sender,
        imageFileBase64Encoded,
        fileName
      );
    }
    this.getMessages();
  }

  #getSendMessageFetchPromise(message) {
    let postData = {
      listenerId: Config.listenerId,
      sender: this.#sender,
      message: message,
    };
    const request = new Request(Config.endpointAddMessage, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postData),
    });

    return fetch(request);
  }

  getMessages(sender = this.#sender) {
    if (!sender) throw Error("Sender oben angeben.");
    this.#sender = sender;
    this.#getMessagesFetchPromise().catch((error) => {
      //do not throw errors from promises
      GlobalErrorHandler.showError(
        `sendMessage api error ${error.message}`,
        "sorry, could not fetch messages :("
      );
    });
  }

  async #getMessagesFetchPromise() {
    log.debug("fetch messages for " + this.#sender);
    let url = Config.endpointGetMessages.replace("|sender|", this.#sender);
    const request = new Request(url);
    const response = await fetch(request);
    const json = await response.json();
    log.debug("raise on messages changed, messages: ");
    this.#onMessagesChanged(json);
  }

  #getUploadImageFetchPromise(
    messageId,
    sender,
    imageFileBase64Encoded = "",
    fileName = ""
  ) {
    if (!sender || !imageFileBase64Encoded || !messageId || !fileName) return;
    let postData = new FormData();
    postData.append(
      "fileName",
      `${Config.listenerId}-${sender}-${messageId}-${fileName}`
    );
    postData.append("encoded", imageFileBase64Encoded);
    let url = Config.endpointAddImage.replace("|id|", messageId);
    const request = new Request(url, {
      method: "POST",
      body: postData,
    });
    return fetch(request)
      .then((response) => console.log(response.status))
      .catch((error) => {
        GlobalErrorHandler.showError(
          `uploadImage api error ${error.message}`,
          "sorry, could not upload the image :("
        );
      });
  }
}

class MessagesView {
  #listDomObject = null;

  constructor() {
    this.#listDomObject = document.getElementById(Config.ids.listChat);
  }

  showMessages(messages) {
    this.#clearMessagesViewList();
    if (messages.length > 0) {
      for (const message of messages) {
        this.#listDomObject.appendChild(this.#createListItem(message));
      }
      //sroll down
      this.#listDomObject.lastChild.scrollIntoView({ behavior: "smooth" });
      //focus input box
      document.getElementById(Config.ids.txtMessage).focus();
    } else {
      GlobalErrorHandler.showError("no messages", "");
    }
  }

  #clearMessagesViewList() {
    this.#listDomObject.replaceChildren();
  }

  #createListItem(message) {
    // create structure
    // <li>
    //   <div class="messageImage">
    //     <img src="path" alt=""/>
    //   </div>
    //   <div class="messageDetails">
    //     <h3>message</h3>
    //     <p>date</p>
    //   </div>
    // </li>
    let li = document.createElement("li");
    let divImage = document.createElement("div");
    divImage.className = "messageImage";
    if (message.imageFilename) {
      let img = document.createElement("img");
      img.src = Config.imagesBaseUrl + message.imageFilename;
      img.alt = "";
      divImage.appendChild(img);
    }
    let divDetails = document.createElement("div");
    divDetails.className = "messageDetails";
    let title = document.createElement("h3");
    title.textContent = message.message;
    divDetails.appendChild(title);
    let details = document.createElement("p");
    let check = message.statusRead ? " ✔️" : "";
    details.textContent = message.datetimeSend.toLocaleString() + check;
    divDetails.appendChild(details);
    li.appendChild(divImage);
    li.appendChild(divDetails);
    return li;
  }
}

class SendView {
  #messageField = null;
  #inputFileField = null;
  #selectedFiles = [];

  constructor() {
    this.#messageField = document.getElementById(Config.ids.txtMessage);
    this.#inputFileField = document.getElementById(Config.ids.inputFile);
    this.#inputFileField.onchange = () => this.#handleFilesChanged();
  }

  bindSendBtnClicked(sendSingleMessageHandler) {
    let btnSend = document.getElementById(Config.ids.btnSend);
    btnSend.addEventListener("click", (event) => {
      let msg = this.#messageField.value;
      let countImages = this.#selectedFiles.length;
      if (msg || countImages > 0) {
        WaitAnimation.show();
        for (let i = 0; i < countImages; i++) {
          if (countImages > 1) msg = `${msg} ${i + 1}/${countImages} `;
          sendSingleMessageHandler(
            msg,
            this.#selectedFiles[i].base64,
            this.#selectedFiles[i].filename
          );
          msg = ""; //only show the message for the first
        }
        this.#resetInput();
        WaitAnimation.hide();
      }
    });
  }

  #handleFilesChanged(handler) {
    if (this.#inputFileField.files.length == 0) {
      return;
    }
    WaitAnimation.show();
    for (let i = 0; i < this.#inputFileField.files.length; i++) {
      //see https://base64.guru/developers/javascript/examples/encode-form-file or https://pqina.nl/blog/convert-a-file-to-a-base64-string-with-javascript/
      let reader = new FileReader();
      //todo show processing animation
      reader.onloadend = () => {
        // Since it contains the Data URI, we should remove the prefix and keep only Base64 string
        const base64String = reader.result.replace(/^data:.+;base64,/, "");
        this.#selectedFiles.push({
          filename: this.#inputFileField.files[i].name,
          base64: base64String, // Base64 encoded string
        });
        WaitAnimation.hide();
      };
      reader.readAsDataURL(this.#inputFileField.files[i]);
    }
  }

  #resetInput() {
    this.#messageField.value = "";
    this.#inputFileField.value = "";
    this.#selectedFiles = [];
  }
}

class Controller {
  #api = null;
  #sender = null;
  #sendView = null;
  #messagesView = null;
  constructor(api, sender, sendView, messagesView) {
    this.#api = api;
    this.#sender = sender;
    this.#sendView = sendView;
    this.#messagesView = messagesView;
    this.#wireUp();

    //initial call, if sender was loaded from cookie
    let senderInfo = this.#sender.getSender();
    if (senderInfo) this.#api.getMessages(senderInfo);
  }

  #wireUp() {
    this.#sender.bindSenderChanged((sender) => {
      this.#api.getMessages(sender);
    });
    this.#api.bindMessagesChanged((messages) => {
      this.#messagesView.showMessages(messages);
    });
    this.#sendView.bindSendBtnClicked(
      (message, imageFileBase64Encoded, imageFilename) => {
        this.#api.sendMessage(message, imageFileBase64Encoded, imageFilename);
      }
    );
  }
}

class GlobalErrorHandler {
  static showError(technical, userfriendly) {
    log.error(technical);
    GlobalErrorHandler.#showSnackbar(userfriendly);
    //@todo extend to send error to server/as mail
  }
  static #showSnackbar(message) {
    var snackBar = document.getElementById("snackbar");
    snackBar.className = "show-bar";
    snackBar.textContent = message;
    setTimeout(function () {
      snackBar.className = snackBar.className.replace("show-bar", "");
    });
  }
}

class WaitAnimation {
  static #waitContainer = document.getElementById("waitContainer");

  static hide() {
    WaitAnimation.#waitContainer.style.display = "none";
  }
  static show() {
    WaitAnimation.#waitContainer.style.display = "block";
  }
}

try {
  let app = new Controller(
    new ChatApi(),
    new Sender(),
    new SendView(),
    new MessagesView()
  );
  WaitAnimation.hide();
} catch (error) {
  GlobalErrorHandler.showError(
    `unknown error ${error.message}`,
    "sorry, something went wrong..."
  );
}
