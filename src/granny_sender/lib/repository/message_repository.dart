import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:image_picker_platform_interface/image_picker_platform_interface.dart';

import '../models/message.dart';
import '../utils/constants.dart';

final repositoryProvider =
    Provider<MessageRepository>((_) => MessageRepository());

class MessageRepository {
  final Dio _dio = Dio(BaseOptions(
      connectTimeout: const Duration(seconds: 30),
      receiveTimeout: const Duration(seconds: 90),
      headers: {
        "Authorization": GrannyMessangerApiConstants.basicAuth,
        "Content-Type": "application/json; charset=UTF-8"
      }));

  Future<List<Message>> getMessages({required String sender}) async {
    if (sender.isEmpty) return Future.value(<Message>[]);
    final response = await _dio
        .get("${GrannyMessangerApiConstants.getMessagesUrl}?sender=$sender");
    if (response.statusCode == GrannyMessangerApiConstants.statusOk) {
      var messages = response.data as List;
      return messages
          .map((messageJson) => Message.fromJson(messageJson))
          .toList();
    }
    throw UnimplementedError(response.data.toString());
  }

  Future<int> add(String sender, String message) async {
    if (sender.isEmpty) return Future.value(-1);
    final response =
        await _dio.post(GrannyMessangerApiConstants.addMessageUrl, data: {
      'listenerId': GrannyMessangerApiConstants.listenerId,
      'sender': sender,
      'message': message
    });
    if (response.statusCode == GrannyMessangerApiConstants.statusOk) {
      var id = response.data as int;
      return id;
    }
    throw UnimplementedError(response.data.toString());
  }

  Future<bool> uploadImage(String sender, int messageId, XFile file) async {
    if (sender.isEmpty || messageId < 0) Future.value(-1);
    String fileName = file.path.split(Platform.pathSeparator).last;
    var url =
        "${GrannyMessangerApiConstants.apiBaseUrl}/messages/$messageId/images";
    final formData = FormData.fromMap({
      'fileName':
          '${GrannyMessangerApiConstants.listenerId}-$sender-$messageId-$fileName',
      'encoded': base64Encode(await file.readAsBytes())
    });
    final response = await _dio.post(url, data: formData);
    if (response.statusCode == GrannyMessangerApiConstants.statusOk) {
      return true;
    }
    throw UnimplementedError(response.data.toString());
  }
}
