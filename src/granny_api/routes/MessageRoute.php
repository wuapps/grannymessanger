<?php

declare(strict_types=1);

require_once './data/MessageDao.php';
require_once './config.php';

class MessageRoute
{
   public function getAllForListener($array): Response
   {
      $response = new Response();
      $response->status = 404;
      if (isset($array[0])) {
         $listenerId = intval($array[0]);
         if (isset($listenerId)) {
            $dao = new MessageDao();
            $response->data = $dao->getAllForListener($listenerId);
            $response->status = 200;
         }
      }
      return $response;
   }

   public function getAllOfSenderToListener($array): Response
   {

      $response = new Response();
      $response->status = 404;
      if (isset($array[0]) && $array[1]) {
         $listenerId = intval($array[0]);
         if (isset($listenerId)) {
            $sender = $array[1];
            if (isset($listenerId) && strlen($sender) > 2) {
               $dao = new MessageDao();
               $response->data = $dao->getAllOfSenderToListener($sender, $listenerId);
               $response->status = 200;
            }
         }
      }
      return $response;
   }

   public function get($array): Response
   {
      $response = new Response();
      $response->status = 404;
      if (isset($array[0])) {
         $id = intval($array[0]);
         if (isset($id)) {
            $dao = new MessageDao();
            $response->data = $dao->get($id);
            $response->status = 200;
         }
      }

      return $response;
   }

   public function add(): Response
   {
      $response = new Response();
      $response->status = 404;
      $json = file_get_contents('php://input');
      $data = json_decode($json);
      if (isset($data->message) && isset($data->sender) && isset($data->listenerId)) {
         $dao = new MessageDao();
         $newId = $dao->add(intval($data->listenerId), $data->sender, $data->message);
         if (isset($newId) && $newId > 0) {
            $response->data = $newId;
            $response->status = 200;
         }
      }

      return $response;
   }

   public function uploadImage($array): Response
   {
      $response = new Response();
      $response->status = 404;
      if (isset($array[0]) && isset($_POST["fileName"]) && isset($_POST["encoded"])) {
         $id = intval($array[0]);
         if (isset($id)) {
            $targetFilename = $this->trimFilename($_POST["fileName"]);
            $encoded = $_POST["encoded"];
            $filecode = base64_decode($encoded);
            if (file_put_contents(IMAGE_FOLDER . $targetFilename, $filecode)) {
               $dao = new MessageDao();
               $dao->setImageFilename($id, $targetFilename);
               $response->status = 200;
            }
         }
      }
      return $response;
   }

   public function updateMessageStatusRead($array): Response
   {
      $response = new Response();
      $response->status = 404;
      //PHP does not know $_PATCH!!!
      $json = file_get_contents('php://input');
      $data = json_decode($json);
      if (isset($array[0]) && isset($data->statusRead)) {
         $id = intval($array[0]);
         $statusRead = intval($data->statusRead);
         if (isset($id) && isset($statusRead)) {
            $dao = new MessageDao();
            if ($dao->updateMessageStatusRead($id, $statusRead)) {
               $response->status = 200;
            }
         }
      }
      return $response;
   }

   private function trimFilename($filename): string
   {
      $plainName = strtolower(pathinfo($filename, PATHINFO_FILENAME));
      $imageFileExtension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
      //remove all special chars etc.
      return preg_replace("/[^a-z0-9-_]/", "", $plainName) . "." . $imageFileExtension;
   }
}
