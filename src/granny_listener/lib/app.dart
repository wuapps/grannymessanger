import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'localization/locale_keys.g.dart';
import 'routes.dart';
import 'utils/styles.dart';
import 'utils/constants.dart';

/// The Widget that configures your application.
class MyApp extends HookConsumerWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp(
      debugShowCheckedModeBanner: GrannyMessangerApiConstants.isDevEnvironment,
      routes: Routes.routes,
      restorationScopeId: 'app',
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      onGenerateTitle: (BuildContext context) => LocaleKeys.title.tr(),
      theme: ThemeData(
          useMaterial3: true, colorScheme: AppStyles.lightAppColorSchema),
      darkTheme: ThemeData.dark(),
    );
  }
}
