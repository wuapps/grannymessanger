// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Message _$$_MessageFromJson(Map<String, dynamic> json) => _$_Message(
      id: json['id'] as int,
      message: json['message'] as String,
      datetimeSend: DateTime.parse(json['datetimeSend'] as String),
      imageFilename: json['imageFilename'] as String,
      statusRead: json['statusRead'] as bool,
    );

Map<String, dynamic> _$$_MessageToJson(_$_Message instance) =>
    <String, dynamic>{
      'id': instance.id,
      'message': instance.message,
      'datetimeSend': instance.datetimeSend.toIso8601String(),
      'imageFilename': instance.imageFilename,
      'statusRead': instance.statusRead,
    };
