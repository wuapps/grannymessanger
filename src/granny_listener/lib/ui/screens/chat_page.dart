import 'package:flutter/material.dart';
import 'package:granny_listener/utils/styles.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../provider/message_provider.dart';
import '../widgets/message_item_widget.dart';

class ChatPage extends HookConsumerWidget {
  ChatPage({super.key});
  late WidgetRef ref;
  final controller = AutoScrollController(axis: Axis.vertical);
  int visibleIndex = 0;
  int maxIndex = 0;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var messages = ref.watch(messageFutureProvider);

    return Scaffold(
        // appBar: AppBar(
        //   title: const Text(LocaleKeys.chatPage_title).tr(),
        // ),
        body: Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        IconButton(
            onPressed: () {
              controller.scrollToIndex(visibleIndex--,
                  preferPosition: AutoScrollPosition.begin);
              if (visibleIndex < 0) {
                visibleIndex = 0;
              }
            },
            icon: const Icon(Icons.arrow_upward, size: 150)),
        IconButton(
            onPressed: () {
              ref.invalidate(messageFutureProvider);
              visibleIndex = 0;
              controller.scrollToIndex(visibleIndex,
                  preferPosition: AutoScrollPosition.begin);
            },
            icon: const Icon(
              Icons.refresh_sharp,
              size: 150,
            )),
        IconButton(
            onPressed: () {
              controller.scrollToIndex(visibleIndex++,
                  preferPosition: AutoScrollPosition.begin);
              if (visibleIndex > maxIndex) {
                visibleIndex = 0;
              }
            },
            icon: const Icon(
              Icons.arrow_downward,
              size: 150,
            )),
      ]),
      Expanded(
          child: messages.when(data: (data) {
        return ListView.builder(
            controller: controller,
            itemCount: data.length,
            itemBuilder: (context, index) {
              maxIndex = data.length - 1;
              return AutoScrollTag(
                key: ValueKey(index),
                controller: controller,
                index: index,
                child: MessageItemWidget(
                  message: data[index],
                  index: index,
                ),
              );
            });
      }, error: (Object error, StackTrace stackTrace) {
        return Center(
          child: Text(error.runtimeType.toString()),
        );
      }, loading: () {
        return const Center(
          child: CircularProgressIndicator(
            strokeWidth: 10,
            backgroundColor: AppStyles.header,
          ),
        );
      })),
    ]));
  }
}
