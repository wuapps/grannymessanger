# API
The architecture is similar to typical REST-Frameworks. You define your routes in App.php, the main entry point of the app. In config.php you define your base url. The access to the database is managed in the dao class. The router finds matching routes. The matching is based on regular expressions.
Make sure the encoding matches the encoding defined in Db.php -- I use utf8mb4.
## Links
- [RESTful web API design](https://learn.microsoft.com/en-us/azure/architecture/best-practices/api-design)