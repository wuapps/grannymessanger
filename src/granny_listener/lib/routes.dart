import 'package:flutter/widgets.dart';

import 'ui/screens/chat_page.dart';

abstract class Routes {
  static const home = '/';

  static Map<String, WidgetBuilder> routes = {home: (_) => ChatPage()};
}
