import 'package:flutter/widgets.dart';

class AppNavigator {
  factory AppNavigator() {
    return _instance;
  }
  AppNavigator._();
  static final AppNavigator _instance = AppNavigator._();

  void pop(
    BuildContext context,
  ) {
    Navigator.of(context).pop();
  }
}
