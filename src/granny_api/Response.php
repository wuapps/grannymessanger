<?php 
declare(strict_types=1);

class Response {
    public int $status = 404;
    public $data = null;

    public function send(){
        if ($this->status === 200){
            header("Content-type: application/json; charset=UTF-8");
            echo json_encode($this->data);
        }
        else {
            http_response_code($this->status);
        }
    }
}