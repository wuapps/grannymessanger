import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../models/message.dart';
import '../repository/message_repository.dart';
import 'sender_provider.dart';

final messageFutureProvider = FutureProvider.autoDispose<List<Message>>((ref) {
  final messageRepository = ref.read(messageRepositoryProvider);
  return messageRepository.getMessages(sender: ref.watch(senderProvider));
});

final messageRepositoryProvider = Provider<MessageRepository>((_) {
  return MessageRepository();
});
