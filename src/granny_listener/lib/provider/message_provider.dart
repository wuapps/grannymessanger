import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../models/message.dart';
import '../repository/message_repository.dart';

final messageFutureProvider = FutureProvider.autoDispose<List<Message>>((ref) {
  final messageRepository = ref.read(messageRepositoryProvider);
  return messageRepository.getMessages();
});

final messageRepositoryProvider = Provider<MessageRepository>((_) {
  return MessageRepository();
});
