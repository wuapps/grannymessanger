// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'message.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Message _$MessageFromJson(Map<String, dynamic> json) {
  return _Message.fromJson(json);
}

/// @nodoc
mixin _$Message {
  int get id => throw _privateConstructorUsedError;
  String get sender => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;
  DateTime get datetimeSend => throw _privateConstructorUsedError;
  String get imageFilename => throw _privateConstructorUsedError;
  bool get statusRead => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MessageCopyWith<Message> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageCopyWith<$Res> {
  factory $MessageCopyWith(Message value, $Res Function(Message) then) =
      _$MessageCopyWithImpl<$Res, Message>;
  @useResult
  $Res call(
      {int id,
      String sender,
      String message,
      DateTime datetimeSend,
      String imageFilename,
      bool statusRead});
}

/// @nodoc
class _$MessageCopyWithImpl<$Res, $Val extends Message>
    implements $MessageCopyWith<$Res> {
  _$MessageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sender = null,
    Object? message = null,
    Object? datetimeSend = null,
    Object? imageFilename = null,
    Object? statusRead = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sender: null == sender
          ? _value.sender
          : sender // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      datetimeSend: null == datetimeSend
          ? _value.datetimeSend
          : datetimeSend // ignore: cast_nullable_to_non_nullable
              as DateTime,
      imageFilename: null == imageFilename
          ? _value.imageFilename
          : imageFilename // ignore: cast_nullable_to_non_nullable
              as String,
      statusRead: null == statusRead
          ? _value.statusRead
          : statusRead // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MessageCopyWith<$Res> implements $MessageCopyWith<$Res> {
  factory _$$_MessageCopyWith(
          _$_Message value, $Res Function(_$_Message) then) =
      __$$_MessageCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String sender,
      String message,
      DateTime datetimeSend,
      String imageFilename,
      bool statusRead});
}

/// @nodoc
class __$$_MessageCopyWithImpl<$Res>
    extends _$MessageCopyWithImpl<$Res, _$_Message>
    implements _$$_MessageCopyWith<$Res> {
  __$$_MessageCopyWithImpl(_$_Message _value, $Res Function(_$_Message) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? sender = null,
    Object? message = null,
    Object? datetimeSend = null,
    Object? imageFilename = null,
    Object? statusRead = null,
  }) {
    return _then(_$_Message(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      sender: null == sender
          ? _value.sender
          : sender // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      datetimeSend: null == datetimeSend
          ? _value.datetimeSend
          : datetimeSend // ignore: cast_nullable_to_non_nullable
              as DateTime,
      imageFilename: null == imageFilename
          ? _value.imageFilename
          : imageFilename // ignore: cast_nullable_to_non_nullable
              as String,
      statusRead: null == statusRead
          ? _value.statusRead
          : statusRead // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Message implements _Message {
  const _$_Message(
      {required this.id,
      required this.sender,
      required this.message,
      required this.datetimeSend,
      required this.imageFilename,
      required this.statusRead});

  factory _$_Message.fromJson(Map<String, dynamic> json) =>
      _$$_MessageFromJson(json);

  @override
  final int id;
  @override
  final String sender;
  @override
  final String message;
  @override
  final DateTime datetimeSend;
  @override
  final String imageFilename;
  @override
  final bool statusRead;

  @override
  String toString() {
    return 'Message(id: $id, sender: $sender, message: $message, datetimeSend: $datetimeSend, imageFilename: $imageFilename, statusRead: $statusRead)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Message &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.sender, sender) || other.sender == sender) &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.datetimeSend, datetimeSend) ||
                other.datetimeSend == datetimeSend) &&
            (identical(other.imageFilename, imageFilename) ||
                other.imageFilename == imageFilename) &&
            (identical(other.statusRead, statusRead) ||
                other.statusRead == statusRead));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, sender, message,
      datetimeSend, imageFilename, statusRead);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MessageCopyWith<_$_Message> get copyWith =>
      __$$_MessageCopyWithImpl<_$_Message>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MessageToJson(
      this,
    );
  }
}

abstract class _Message implements Message {
  const factory _Message(
      {required final int id,
      required final String sender,
      required final String message,
      required final DateTime datetimeSend,
      required final String imageFilename,
      required final bool statusRead}) = _$_Message;

  factory _Message.fromJson(Map<String, dynamic> json) = _$_Message.fromJson;

  @override
  int get id;
  @override
  String get sender;
  @override
  String get message;
  @override
  DateTime get datetimeSend;
  @override
  String get imageFilename;
  @override
  bool get statusRead;
  @override
  @JsonKey(ignore: true)
  _$$_MessageCopyWith<_$_Message> get copyWith =>
      throw _privateConstructorUsedError;
}
