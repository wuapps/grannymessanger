import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final senderProvider = StateNotifierProvider<SenderNotifier, String>(
  (ref) => SenderNotifier(),
);

class SenderNotifier extends StateNotifier<String> {
  late SharedPreferences prefs;

  Future _init() async {
    prefs = await SharedPreferences.getInstance();
    state = getSender();
  }

  SenderNotifier() : super('') {
    _init();
  }

  static const String _keySender = 'sender';

  String getSender() {
    return prefs.getString(_keySender) ?? '';
  }

  Future<bool> setSender(String name) {
    var sender = '';
    sender = name;
    state = sender;
    return prefs.setString(_keySender, sender);
  }
}
