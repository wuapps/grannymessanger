<?php

declare(strict_types=1);

//inspired by https://github.com/devcoder-xyz/php-router/blob/master/src/Route.php

class Route
{
    private string $matchExpression;
    private string $method;
    private string $handlerClass;
    private string $handlerMethod;
    private $attributes = [];

    public function __construct(string $method, string $matchExpression, $handlerClass, string $handlerMethod)
    {
        $method = strtolower($method);
        if ($method !== 'post' && $method !== 'get' && $method !== 'patch')
            throw new Exception("method not supported");

        $this->matchExpression = $this->trimPath($matchExpression);
        $this->handlerClass = $handlerClass;
        $this->handlerMethod = $handlerMethod;
        $this->method = $method;
    }

    /**
     * remove leading and ending / and whitespaces
     */
    private function trimPath(string $path)
    {
        return trim(trim($path), "/");
    }

    public function isMatch(string $path, string $method): bool
    {
        $method = strtolower($method);
        if ($method !== $this->getMethod()) {
            return false;
        }
        $path = $this->trimPath($path);
        if ($this->matchExpression === $path) {
            return true;
        }

        $regex = $this->matchExpression;
        //complete match with #^ $#
        if (preg_match('#^' . $regex . '$#', trim($path), $matches)) {
            for ($i = 1; $i < count($matches); $i++) { //first match is the complete path
                $this->attributes[] = $matches[$i];
            }
            return true;
        }
        return false;
    }

    public function getHandlerClass()
    {
        return $this->handlerClass;
    }

    public function getHandlerMethod(): string
    {
        return $this->handlerMethod;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function hasAttributes(): bool
    {
        return $this->attributes !== [];
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
