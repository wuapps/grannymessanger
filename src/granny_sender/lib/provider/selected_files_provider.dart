import 'package:hooks_riverpod/hooks_riverpod.dart';


import 'package:image_picker_platform_interface/image_picker_platform_interface.dart';

final selectedFilesProvider = StateNotifierProvider<SelectedFiles, List<XFile>>((ref) {
  return SelectedFiles();
});

class SelectedFiles extends StateNotifier<List<XFile>> {
  SelectedFiles() : super([]);
  void selectedFiles(List<XFile> selectedFiles) {
    state = selectedFiles;
  }

  void clearFiles() {
    state = [];
  }
}
