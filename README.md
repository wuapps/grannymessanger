# Granny Messanger

I want to implement a simple messanger app for elderly people with no smart phone. The application consists of three parts

- Server: Simple REST-API based on PHP and mariadb, that will run on a cheap web space. Use basic security with .htaccess
- GrannySender: Simple UI based on Flutter to send messages and images to one person (granny).
- GrannyListener: Simple UI, that may run on a FireTV stick and shows all messages and images. Navigation uses up and down keys only.
- GrannySenderWeb: Web sender application based on plain javascript/html/css -- the flutter web app caused many troubles and just did not work.

GrannySender should run on Android and as a web app (GrannySenderWeb), iphone optional. GrannyListener should run on Android (TV).

## Setup/Build
All server specific/private settings are not in the repository. The files are provided as .template-files. Hence, before you start developing you have to remove the .template from those files and fill them with your server data. Commits before 2023-03-14 might miss those template files ...
## Granny API

The architecture is similar to typical REST-Frameworks. You define your routes in App.php, the main entry point of the app. In config.php you define your base url. The access to the database is managed in the dao class. The router finds matching routes. The matching is based on regular expressions.
Make sure the encoding matches the encoding defined in Db.php -- I use utf8mb4.

### ToDos

- add logging, e.g. as described in https://stackify.com/display-php-errors/

## Flutter Apps

ToDo

- work with viewmodels/controlers as described in, i.e. one viewmodel per widget unit
  - https://medium.com/@mxiskw/flutter-pragmatic-architecture-using-riverpod-123ae11a8267
  - https://bettercoding.dev/flutter/simple-app-architecture-riverpod/
- better error/exception handling
- add dio logger

### Code Generation
- freezed
```
flutter pub run build_runner build
```
- easy_localization
```
flutter pub run easy_localization:generate -S assets/translations -f keys -O lib/localization -o locale_keys.g.dart
```


### Libs

The apps use the following libraries

- [dio](https://pub.dev/packages/dio): A powerful Http client for Dart, which supports Interceptors, FormData, Request Cancellation, File Downloading, Timeout etc.
- [equatable](https://pub.dev/packages/equatable): A Dart package that helps to implement value based equality without needing to explicitly override == and hashCode.
- [logger](https://pub.dev/packages/logger): Small, easy to use and extensible logger which prints beautiful logs.
- [cached_network_image](https://pub.dev/packages/cached_network_image): Flutter library to load and cache network images. Can also be used with placeholder and error widgets.
- [hooks_riverpod](https://pub.dev/packages/hooks_riverpod): State management/data binding
  - [A minimalist guide to Riverpod](https://itnext.io/a-minimalist-guide-to-riverpod-4eb24b3386a1)
  - [Flutter Riverpod 2.0: The Ultimate Guide](https://codewithandrea.com/articles/flutter-state-management-riverpod/)
  - use hooks to reduce boilerplate code. Hooks exist for one reason: increase the code-sharing between widgets by removing duplicates.
    [Hook is a simple way to cache an instance of an object during the lifecycle of your widget.](https://medium.com/flutter-community/flutter-hooks-say-goodbye-to-statefulwidget-and-reduce-boilerplate-code-8573d4720f9a).
    All examples provided by riverpod use hooks.
- [freezed](https://pub.dev/packages/freezed): reduced boilerplate code for data classes.
- [shared_preferences](https://pub.dev/packages/shared_preferences): Wraps platform-specific persistent storage for simple data
  - [Store key-value data on disk](https://docs.flutter.dev/cookbook/persistence/key-value)
- [easy_localization](https://pub.dev/packages/easy_localization)

In general I prefere a feature oriented structure in my apps. However, the app is very simply, hence I go for a layered approach.

### Implementation steps

- App creation with VS Code new flutter project skeleton app
- replace com.example. with my namespace
- add libs according to their setup
- use [material 3](https://blog.codemagic.io/migrating-a-flutter-app-to-material-3/)
- add german localization app_de.arb
- remove [debug banner](https://www.fluttercampus.com/guide/147/how-to-remove-debug-banner-tag-from-debugging-flutter-app/)
- refactor to use hooks_riverpod
- connect to granny api
- follow instructions of https://docs.flutter.dev/deployment/android for build of android
- switch to easy_localization

### build
- open android folder in Android Studío
  - build --> select build variant
  - build --> build apk is in folder build\app\outputs\apk\release
 

### Granny Sender

Additional libs/links

- [flutter_chat_ui](https://pub.dev/packages/flutter_chat_ui) -- maybe later (currently too much dependencies and features we do not need)
- use basic implementation as described in [How to build a chat application in Flutter with Firebase](https://blog.logrocket.com/how-to-build-chat-application-flutter-firebase/)
- [image_picker](https://pub.dev/packages/image_picker)

### Granny Listener
Unfortunately Fire-TV does not support autostart and my mom was not able to navigate to the app :(
My old unused notebook has not enough power to run the app, hence I focus on my old tablet with automdroid for autostart and voice access app (app stoe) with easy pronouncable buttons. This app is not always active ...


## ToDos

- adapt listener to tv screen
- ux improvements after usability test
- add caching as discussed in https://codewithandrea.com/articles/flutter-riverpod-data-caching-providers-lifecycle/
