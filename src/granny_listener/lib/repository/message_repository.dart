import 'package:dio/dio.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../models/message.dart';
import '../utils/constants.dart';
import '../utils/logging.dart';

final repositoryProvider =
    Provider<MessageRepository>((_) => MessageRepository());

class MessageRepository {
  final log = getLogger();
  final Dio _dio = Dio(BaseOptions(
      connectTimeout: const Duration(seconds: 5),
      receiveTimeout: const Duration(seconds: 10),
      headers: {"Authorization": GrannyMessangerApiConstants.basicAuth}));
  Future<List<Message>> getMessages() async {
    log.d("getMessages");
    final response = await _dio
        .get("${GrannyMessangerApiConstants.messagesUrlForListener}/");
    log.d(response.data.toString());
    if (response.statusCode == GrannyMessangerApiConstants.statusOk) {
      var messages = response.data as List;
      return messages
          .map((messageJson) => Message.fromJson(messageJson))
          .toList();
    }
    throw UnimplementedError(response.data.toString());
  }

  Future<bool> updateMessageAsRead(int id) async {
    log.d("update message as read");
    var url = "${GrannyMessangerApiConstants.messagesUrl}/$id/";
    final response = await _dio.patch(url, data: {'statusRead': 1});
    if (response.statusCode == GrannyMessangerApiConstants.statusOk) {
      return true;
    }
    throw UnimplementedError(response.data.toString());
  }
}
