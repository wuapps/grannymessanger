<?php

declare(strict_types=1);

require_once 'Route.php';
require_once 'config.php';
class Router
{
    private $routes = [];
    private $baseUri = BASE_URI;

    public function add(string $method, string $path, $handlerClass, string $handlerMethod): void
    {
        $this->routes[] = new Route($method, $path, $handlerClass, $handlerMethod);
    }

    public function findRoute($requestUri, $requestMethod)
    {
        $path = str_replace($this->baseUri, '', $requestUri);
        foreach ($this->routes as $route) {
            if ($route->isMatch($path, $requestMethod)) {
                return $route;
            }
        }

        return null;
    }
}
