<?php

declare(strict_types=1);

require_once 'Message.php';
require_once 'Db.php';

class MessageDao extends Db
{
    public function getAllForListener(int $listenerId): array
    {
        $sql = "SELECT id, sender, message, datetimeSend, imageFilename, statusRead FROM messages where listenerId =:listenerId ORDER BY statusRead asc, sender asc, datetimeSend desc";
        $statement = $this->database->prepare($sql);
        $statement->bindValue(':listenerId', $listenerId, PDO::PARAM_INT);
        $statement->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Message');
        $statement->execute();
        $messages = $statement->fetchAll();
        $statement->closeCursor();
        return $messages;
    }

    public function getAllOfSenderToListener(string $sender, int $listenerId): array
    {
        $sql = "SELECT id, message, datetimeSend, imageFilename, statusRead FROM messages where sender=:sender and listenerId =:listenerId";
        $statement = $this->database->prepare($sql);
        $statement->bindValue(':sender', $sender, PDO::PARAM_STR);
        $statement->bindValue(':listenerId', $listenerId, PDO::PARAM_INT);
        $statement->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Message');
        $statement->execute();
        $messages = $statement->fetchAll();
        $statement->closeCursor();
        return $messages;
    }

    public function updateMessageStatusRead(int $id, int $status): bool
    {
        $sql = "UPDATE messages SET statusRead=:statusRead WHERE id=:id";
        $statement = $this->database->prepare($sql);
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->bindValue(':statusRead', $status, PDO::PARAM_INT);
        return ($statement->execute());
    }

    public function add(int $listenerId, string $sender, string $message): int
    {
        $sql = "INSERT INTO messages(listenerId, sender, message) VALUES (:listenerId, :sender, :msg)";
        $statement = $this->database->prepare($sql);
        $statement->bindValue(':listenerId', $listenerId, PDO::PARAM_INT);
        $statement->bindValue(':sender', $sender, PDO::PARAM_STR);
        $statement->bindValue(':msg', $message, PDO::PARAM_STR);
        $statement->execute();
        $insertId = $this->database->lastInsertId();
        return intval($insertId);
    }

    public function get(int $id): Message
    {
        $sql = "SELECT * FROM messages  WHERE id=:id";

        $statement = $this->database->prepare($sql);
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Message');
        $statement->execute();
        $message = $statement->fetch();
        $statement->closeCursor();
        return $message;
    }

    public function setImageFilename(int $id, string $filename): void
    {
        $statement = $this->database->prepare("UPDATE messages SET imageFilename=:imageFilename  WHERE id=:id");
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->bindValue(':imageFilename', $filename, PDO::PARAM_STR);
        $statement->execute();
    }
}
