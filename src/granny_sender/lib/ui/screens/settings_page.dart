import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../localization/locale_keys.g.dart';
import '../../provider/sender_provider.dart';

class SettingsPage extends HookConsumerWidget {
  SettingsPage({super.key});
  final Key _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    String name = '';
    final navigator = Navigator.of(context);
    return PopScope(
        canPop: false,
        onPopInvokedWithResult: (bool didPop, Object? result) async {
          if (!didPop) {
            if (!_isValidName(name)) return;
            await ref.read(senderProvider.notifier).setSender(name);
            navigator.pop();
          }
        },
        child: SafeArea(
            bottom: true,
            top: true,
            left: true,
            right: true,
            child: Scaffold(
                appBar: AppBar(
                    title: const Text(LocaleKeys.settingsPage_title).tr()),
                body: Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(5),
                    child: Form(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.always,
                        child: TextFormField(
                          initialValue:
                              ref.read(senderProvider.notifier).getSender(),
                          onChanged: (value) => name = value,
                          decoration: InputDecoration(
                              labelText:
                                  LocaleKeys.settingsPage_inputNameHint.tr()),
                          validator: (value) {
                            if (_isValidName(value)) {
                              return null;
                            } else {
                              return LocaleKeys.settingsPage_inputNameError
                                  .tr();
                            }
                          },
                        ))))));
  }

  bool _isValidName(String? value) {
    return (value != null &&
        value.isNotEmpty &&
        RegExp(r'^[a-zA-Z0-9]{3,8}$').hasMatch(value));
  }
}
