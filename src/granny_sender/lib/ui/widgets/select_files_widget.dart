import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:image_picker_platform_interface/image_picker_platform_interface.dart';

import '../../provider/selected_files_provider.dart';

// Define state providers

class SelectFilesWidget extends HookConsumerWidget {
  const SelectFilesWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final imageFiles = ref.watch(selectedFilesProvider);

    return IconButton(
      onPressed: () async {
        final picker = ImagePickerPlatform.instance;
        // Pick an image
        final List<XFile> pickedFiles = await picker.getMultiImageWithOptions();
        ref.read(selectedFilesProvider.notifier).selectedFiles(pickedFiles);
      },
      icon: imageFiles.isEmpty
          ? const Icon(Icons.image_rounded)
          : imageFiles.length == 1
              ? const Icon(Icons.image_rounded)
              : const Icon(Icons.numbers_rounded),
    );
  }
}
