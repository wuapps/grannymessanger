# Contributing to vr-koni-trainier

## I want to report a problem or ask a question or submit a feature request

Before submitting a new GitLab issue, please make sure to

- Search for [existing GitLab issues](https://gitlab.com/utrapp/vr-koni-trainer/-/issues)

If the above doesn't help, please [submit an issue](https://gitlab.com/utrapp/vr-koni-trainer/-/issues) on GitLab and use the templates provided.

## I want to contribute to vr-koni-trainier

Welcome and happy coding :)

## I want to help work on vr-koni-trainier by reviewing issues and PRs

Thanks! We would really appreciate the help!

## Code of Conduct

Help us keep _vr-koni-trainier_ open and inclusive. Please read and follow our [Code of Conduct][code of conduct].

## Above All, Thanks for Your Contributions

Thank you for reading to the end, and for taking the time to contribute to the project! If you include the 🔑 emoji at the top of the body of your issue or pull request, we'll know that you've given this your full attention and are doing your best to help!

## Links

[code of conduct]: CODE_OF_CONDUCT.md
[license]: LICENSE
