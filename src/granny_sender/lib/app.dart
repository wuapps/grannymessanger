import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'localization/locale_keys.g.dart';
import 'provider/sender_provider.dart';
import 'routes.dart';
import 'utils/colors.dart';
import 'utils/constants.dart';

/// The Widget that configures your application.
class MyApp extends HookConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp(
      debugShowCheckedModeBanner: GrannyMessangerApiConstants.isDevEnvironment,
      routes: Routes.routes,
      initialRoute:
          ref.watch(senderProvider).isEmpty ? Routes.settingsPage : Routes.home,
      restorationScopeId: 'app',
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      onGenerateTitle: (BuildContext context) => LocaleKeys.title.tr(),
      theme: ThemeData(
          useMaterial3: true, colorScheme: AppColors.lightAppColorSchema),
      darkTheme: ThemeData.dark(
        useMaterial3: true,
      ),
    );
  }
}
