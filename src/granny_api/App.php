<?php

declare(strict_types=1);


require_once 'Router.php';
require_once 'Response.php';
require_once 'routes/MessageRoute.php';

// We might not have full access to the web server, hence no mod_rewrite etc
// Routes are of the type get|post|push.../index.php?api=messages/1
// post handles json data only

// be careful with the order, first match wins!
//first paramenter is listener, i.e. the person receiving the messages
$routes = new Router();
//e.g. listeners/1/messages --> fetch all messages of listener with id 1
$routes->add('get', 'listeners/(\d+)/messages', MessageRoute::class, 'getAllForListener');
//get message with given id, e.g. messages/104
$routes->add('get', 'messages/(\d+)', MessageRoute::class, 'get'); //must be before w+ as it is more specific
//get all messages of sender to listener, e.g. listeners/1/messages?sender=ute all messages from ute to listener with id 1
//? needs to be escaped as it is a special char in regular expressions
$routes->add('get', 'listeners/(\d+)/messages\?sender=(\w+)', MessageRoute::class, 'getAllOfSenderToListener');
//update the statusRead of a given message e.g. messages/1 with formfield statusRead
$routes->add('patch', 'messages/(\d+)', MessageRoute::class, 'updateMessageStatusRead'); //must be before w+ as it is more specific
//adds a new message from sender to given listener, e.g. listeners/1/messages/ and json {sender=ute, message=hi}
$routes->add('post', 'messages', MessageRoute::class, 'add');
//add an image to a message with given id
$routes->add('post', 'messages/(\d+)/images', MessageRoute::class, 'uploadImage');
// $routes->add('get', 'customers/(\d+)/orders/(\d+)', MessageRoute::class, 'get'); will match with two numerical attributes

$response = new Response();
try {
    $route = $routes->findRoute($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
    if (isset($route)) {
        $handlerClassName = $route->getHandlerClass();
        $handlerMethodName = $route->gethandlerMethod();
        $attributes = $route->getAttributes();
        $controller = new $handlerClassName();
        $response = $controller->$handlerMethodName($attributes);
    } else {
        $response->status = 501;
    }
    $response->send();
} catch (Exception $e) {
    header("Content-type: text/plain; charset=UTF-8");
    echo $e->getMessage();
}
