import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:granny_sender/provider/message_provider.dart';
import 'package:granny_sender/provider/selected_files_provider.dart';
import 'package:granny_sender/provider/sender_provider.dart';
import 'package:granny_sender/ui/widgets/select_files_widget.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:image_picker_platform_interface/image_picker_platform_interface.dart';

import '../../provider/files_provider.dart';
import '../../utils/colors.dart';
import '../../localization/locale_keys.g.dart';

class SendWidget extends HookConsumerWidget {
  const SendWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    String sender = ref.watch(senderProvider);
    String message = "";
    final isLoading = ref.watch(isLoadingProvider);
    final uploadProgress = ref.watch(uploadProgressProvider);
    final totalFiles = ref.watch(totalFilesProvider);
    final TextEditingController messageTextController =
        TextEditingController(text: message);

    return Row(children: [
      const SelectFilesWidget(),
      Flexible(
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextField(
                onChanged: (value) => message = value,
                controller: messageTextController,
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 4,
                maxLength: 400,
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: AppColors.textInputBorder, width: 2.0),
                      borderRadius: BorderRadius.circular(10.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: AppColors.textInputBorder, width: 2.0),
                      borderRadius: BorderRadius.circular(10.0)),
                  hintText: sender.isEmpty
                      ? LocaleKeys.chatPage_hintTextMessage.tr()
                      : LocaleKeys.chatPage_hintTextMessage.tr(),
                ),
              ))),
      IconButton(
          onPressed: () {
            _sendImageMessages(sender, message, ref);
            messageTextController.clear();
            message = "";
          },
          icon: const Icon(Icons.send_rounded)),
      if (isLoading)
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const CircularProgressIndicator(), // Show progress indicator
            const SizedBox(height: 8),
            Text('$uploadProgress of $totalFiles uploaded'),
          ],
        ),
    ]);
  }

  void _sendImageMessages(String sender, String message, WidgetRef ref) async {
    final messageRepository = ref.read(messageRepositoryProvider);
    List<XFile> imageFiles = ref.read(selectedFilesProvider);
    var isLoadingStateNotifier = ref.read(isLoadingProvider.notifier);
    var totalFilesStateNotifier = ref.read(totalFilesProvider.notifier);
    var countImages = imageFiles.length;
    isLoadingStateNotifier.state = true;
    if (message.isNotEmpty || countImages > 0) {
      for (int i = 0; i < countImages; i++) {
        if (countImages > 1) {
          message = "$message ${i + 1}/$countImages";
        }
        var imageFile = imageFiles[i];
        int id = await messageRepository.add(sender, message);
        await messageRepository.uploadImage(sender, id, imageFile);
        message = ""; // do not repeat the message for each image
        totalFilesStateNotifier.state = i;
      }
      ref.read(selectedFilesProvider.notifier).clearFiles();
      ref.invalidate(messageFutureProvider);
      isLoadingStateNotifier.state = false;
    }
  }
}
