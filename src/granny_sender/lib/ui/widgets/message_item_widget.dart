import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

import '../../models/message.dart';
import '../../utils/constants.dart';

class MessageItemWidget extends HookConsumerWidget {
  final Message message;

  const MessageItemWidget({super.key, required this.message});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Card(
      elevation: 0,
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            leading: _handleMessageImage(message.imageFilename),
            trailing: message.statusRead
                ? const Icon(
                    Icons.done_all,
                    color: Colors.blue,
                    size: 25.0,
                  )
                : null,
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Text(message.message,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18))),
            ),
            subtitle: Center(
                child: Text(
                    DateFormat.yMd().add_jm().format(message.datetimeSend),
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 15))),
          )),
    );
  }

  Widget? _handleMessageImage(String imageFilename) {
    if (imageFilename.isEmpty) return null;
    return CachedNetworkImage(
      placeholder: (context, url) => const CircularProgressIndicator(),
      errorWidget: (context, url, error) => const Icon(Icons.error),
      imageUrl:
          GrannyMessangerApiConstants.imagesBaseUrl + message.imageFilename,
      height: 100,
    );
  }
}
