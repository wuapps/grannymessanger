<?php

declare(strict_types=1);

const BASE_URI = '/granny-messanger/private/api/App.php/';
const IMAGE_URI = '/granny-messanger/images/';
const IMAGE_FOLDER = '../../images/';
