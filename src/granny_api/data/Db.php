<?php

declare(strict_types=1);

abstract class Db
{
    public PDO $database;

    public function __construct()
    {
        // activate full error checking
        error_reporting(E_ALL);

        // open database
        require_once 'pwd.php'; // read account data
        $dsn = "mysql:host=$host;dbname=$database;charset=utf8mb4";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false
        ];
        $this->database = new PDO($dsn, $user, $pwd, $options);
    }
}
