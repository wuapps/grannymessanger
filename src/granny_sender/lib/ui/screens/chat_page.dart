import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:granny_sender/provider/message_provider.dart';
import 'package:granny_sender/ui/widgets/message_item_widget.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../localization/locale_keys.g.dart';
import '../../navigation.dart';
import '../widgets/send_widget.dart';

class ChatPage extends HookConsumerWidget {
  const ChatPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final messages = ref.watch(messageFutureProvider);
    final scrollController = AutoScrollController(axis: Axis.vertical);
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (scrollController.hasClients &&
          messages.hasValue &&
          messages.value!.isNotEmpty) {
        var x = messages.value!.length - 1;
        scrollController.scrollToIndex(x,
            preferPosition: AutoScrollPosition.end);
      }
    });
    return SafeArea(
        bottom: true,
        top: true,
        left: true,
        right: true,
        child: Scaffold(
            appBar: AppBar(
              title: const Text(LocaleKeys.chatPage_title).tr(),
              actions: <Widget>[
                IconButton(
                  onPressed: () => AppNavigator().navToSettingsPage(context),
                  icon: const Icon(Icons.settings),
                ),
              ],
            ),
            body: Column(children: [
              Expanded(
                  child: messages.when(data: (data) {
                return ListView.builder(
                    controller: scrollController,
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return AutoScrollTag(
                          key: ValueKey(index),
                          controller: scrollController,
                          index: index,
                          child: MessageItemWidget(
                            message: data[index],
                          ));
                    });
              }, error: (Object error, StackTrace stackTrace) {
                return Center(
                  child: Text(error.runtimeType.toString()),
                );
              }, loading: () {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              })),
              const SendWidget(),
            ])));
  }
}
