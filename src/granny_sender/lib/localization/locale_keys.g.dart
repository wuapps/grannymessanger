// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const title = 'title';
  static const settingsPage_title = 'settingsPage.title';
  static const settingsPage_inputNameError = 'settingsPage.inputNameError';
  static const settingsPage_inputNameHint = 'settingsPage.inputNameHint';
  static const settingsPage = 'settingsPage';
  static const chatPage_title = 'chatPage.title';
  static const chatPage_hintTextMessageEmptySender = 'chatPage.hintTextMessageEmptySender';
  static const chatPage_hintTextMessage = 'chatPage.hintTextMessage';
  static const chatPage = 'chatPage';

}
